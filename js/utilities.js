/*******************************************************************************************/
/********************************** UTILITIES METHODS **********************************/
/*******************************************************************************************/
function saveData(name, data) {

    var jsonData = JSON.stringify(data); //paverciam i eilute
    window.localStorage.setItem(name, jsonData);
}

function loadData(name) {
    
    var data = window.localStorage.getItem(name); //duomenys is localStorage
    return JSON.parse(data);
}

function objectivyData(title, firstName, lastName, phone) {

	var data = new Object();
		data.title = title;
		data.firstName = firstName;
		data.lastName = lastName;
		data.phone = phone;

	return data;

}